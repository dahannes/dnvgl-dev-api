const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')
const fs = require('fs')
const mailer = require('express-mailer');
const morgan = require('morgan')
const multer = require('multer')



// const diskDB = require('diskdb');
// const dbPath = __dirname + "/db"
// const db = diskDB.connect(dbPath, ['surveys', 'findings']);
//
// const survey = {
//   _id: '0001',
//   title: 'Some Survey Title',
//   vessel: 'Name of Vessel'
// }
//
// db.surveys.save(survey)
//
// console.log(db.surveys.find(), db.surveys.count())

const port = process.env.PORT || 5000


// Setup Disk Storage
const storageDisk = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, __dirname + '/data/files')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
const uploadDisk = multer({storage: storageDisk})

//  Setup Memory Storage
const storageMemory = multer.memoryStorage()
const uploadBuffer = multer({ storage: storageMemory })

// init app
const app = express()



app.set('view engine', 'pug')


// apply middleware
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

// simple http request logger
app.use(morgan('tiny'))

app.use(cors())

app.get('/', function (req, res) {
  res.send('IT WORKS!')
});

app.get('/jobs', function (req, res) {
  fs.readFile(__dirname + "/data/jobs.json", 'utf8', function(err, jobs ) {
    if (err) {
      console.log(err)
    }
    jobs = JSON.parse(jobs)
    res.json(jobs)
  })
})


// /*
//   Retrieve inspections from server
// */
// app.get('/inspections', function (req, res) {
//   fs.readFile(__dirname + "/data/inspections.json", 'utf8', function(err, inspections ) {
//     if (err) {
//       if (err.code === 'ENOENT') {
//         return  fs.writeFileSync(__dirname + "/data/inspections.json", '[]', 'utf8', function(err) {
//           if (err) {
//             throw err
//           }
//         })
//       }
//       throw err
//     }
//     inspections = JSON.parse(inspections)
//     res.json({inspections})
//   });
// });
//
//
// /*
//   Write inspections to server
// */
// app.post('/inspections', function (req, res) {
//   fs.readFile(__dirname + "/data/inspections.json", 'utf8', function(err, inspections ) {
//     if (err) {
//       console.error(err)
//     }
//     const data = JSON.stringify([...JSON.parse(inspections), req.body.payload])
//     fs.writeFile(__dirname + '/data/inspections.json', data, 'utf-8', function (err) {
//       if (err) throw err
//       res.json('POST inspections success')
//     });
//
//   });
// });


/*
  Sending E-Mail
*/

mailer.extend(app, {
  from: 'johannes.siedler@gmx.de',
  host: 'smtp.gmx.de', // hostname
  secureConnection: true, // use SSL
  port: 465, // port for secure SMTP
  transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
  auth: {
    user: 'johannes.siedler@gmx.de',
    pass: 'n4chBAR_99'
  }
});

app.post('/email', uploadBuffer.single('file'), function (req, res, next) {
  const { to, title, description } = req.body

  app.mailer.send('email', {
    to,
    subject: title,
    title,
    description,
    attachments: [
      {
        fileName: req.file.originalname,
        contents: req.file.buffer
      }
    ]
  }, function (err) {
    if (err) {
      // handle error
      console.log(err);
      res.send('There was an error sending the email');
      return;
    }
    res.send('Email Sent');
  });
});


/*
  Handle single file upload
*/
app.post('/file', uploadDisk.single('file'), function (req, res) {
  console.log('file:\n', req.file, '\n\nbody:\n', req.body)
  if (!req.file) {
    return res.status(400).send('No files were uploaded.');
  }
  res.json({_id: req.body._id, path: '/api/' + req.file.path.split('/data/')[1]})
});

/*
  Return Image File
*/
app.get('/files/:fileName', function (req, res) {
  // TODO: handle authentication
  res.sendFile(req.params.fileName, { root: __dirname + '/data/files/' })
})



/*
  Findings Endpoints
*/

app.get('/findings', function(req, res) {
  res.json('GET findings success')
})

app.post('/finding', function(req, res) {
  res.json('POST finding success')
})

app.put('/finding', function(req, res) {
  res.json('PUT finding success')

})

app.delete('/finding', function(req, res) {
  res.json('DELETE finding success')
})


/*
  Run the development API server
*/
app.listen(port, function () {
  console.log('Example app listening on port ' + port);
});
